﻿using System;
using System.Collections.Generic;

[Serializable]
public class SaveData
{
    public String notice = "PLEASE DON'T HACK THIS SAVE FILE OR I'LL BE SAD :(";
    public List<LifeChestSaveData> lifeChests = new List<LifeChestSaveData>();
}

[Serializable]
public class LifeChestSaveData
{
    public float posX;
    public float posY;
    public float posZ;
    public Dictionary<int, int> inventory;
}
