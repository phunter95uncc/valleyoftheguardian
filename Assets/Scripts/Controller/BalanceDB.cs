﻿using System.Collections.Generic;

public static class BalanceDB
{
    public const int                                    PLAYER_MAX_HEALTH = 100;
    public const int                                    WALL_MAX_HEALTH = 100;
    public const float                                  TREES_PER_UNIT = 4;
    public static Dictionary<EnemyColor, EnemyStats>    ENEMY_STATS;
    public const float                                  BASIC_TOWER_DAMAGE = 20;

    static BalanceDB()
    {
        ENEMY_STATS = new Dictionary<EnemyColor, EnemyStats>();
        ENEMY_STATS.Add(EnemyColor.PURPLE, new EnemyStats(10.0f, 20.0f, 10));
        ENEMY_STATS.Add(EnemyColor.BLUE, new EnemyStats(15.0f, 40.0f, 15));
        ENEMY_STATS.Add(EnemyColor.YELLOW, new EnemyStats(15.0f, 80.0f, 20));
        ENEMY_STATS.Add(EnemyColor.ORANGE, new EnemyStats(20.0f, 100.0f, 25));
        ENEMY_STATS.Add(EnemyColor.RED, new EnemyStats(25.0f, 130.0f, 30));
    }
}
