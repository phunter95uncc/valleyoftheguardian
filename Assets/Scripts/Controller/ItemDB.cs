﻿using static ItemType;
using System;
using System.Collections.Generic;

public static class ItemDB
{
    public static Dictionary<int, Item> ITEMS = new Dictionary<int, Item>();

    static ItemDB()
    {
        RegisterItem(new Item(0, "Life Dust", COLLECTABLE, null, "Essense of life. Obtained from defeating enemies, and used to build towers."));
        RegisterItem(new Item(1, "Wood", COLLECTABLE, null, "Comes from trees."));
        RegisterItem(new Item(2, "Stone", COLLECTABLE, null, "Comes from the ground."));
        RegisterItem(new Item(3, "Ice", COLLECTABLE, null, "Found deep in the caves. Very cold."));

        RegisterItem(new Item(100, "Basic Tower", PLACEABLE,
            new Recipe(new RecipeFacet[] { new RecipeFacet(2, 100), new RecipeFacet(0, 100) }),  // Make from wood until stone is implemented.
            "Simplest tower. Shoots damaging orbs at enemies."));
        RegisterItem(new Item(101, "Snow Fighter", PLACEABLE,
            new Recipe(new RecipeFacet[] { new RecipeFacet(3, 60), new RecipeFacet(0, 200) }),  // Make from wood until stone is implemented.
            "Snow man. Freezes enemies temporarily."));
        RegisterItem(new Item(200, "Life Chest", PLACEABLE,
            new Recipe(new RecipeFacet[] { new RecipeFacet(1, 300), new RecipeFacet(0, 350) }),
            "Stores items, even after death..."));
    }

    public static void RegisterItem(Item item)
    {
        ITEMS.Add(item.id, item);
    }
}

public class Item
{
    public int id;
    public String name;
    public ItemType type;
    public Recipe recipe;
    public String description;

    public Item(int id, String name, ItemType type, Recipe recipe, String description = "")
    {
        this.id = id;
        this.name = name;
        this.type = type;
        this.recipe = recipe;
        this.description = description;
    }
}

public enum ItemType
{
    COLLECTABLE,    // Items you simply collect and craft with
    USEABLE,        // Items you consume 
    PLACEABLE       // Items you place
}

public class Recipe
{
    public RecipeFacet[] facets;

    public Recipe(RecipeFacet[] facets)
    {
        this.facets = facets;
    }
}

public class RecipeFacet
{
    public int itemID;
    public int count;

    public RecipeFacet(int itemID, int count)
    {
        this.itemID = itemID;
        this.count = count;
    }
}
