﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Menu currentMenu;
    public Dictionary<Menu, Canvas> menuMap = new Dictionary<Menu, Canvas>();
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController FPSController;

    // HUD
    public Canvas hudCanvas;
    public Text PlayerHealthText;
    public Text WallHealthText;
    public Text LifeDustInvText;
    public Text WoodInvText;
    public Text StoneInvText;
    public Transform hotbar;
    private List<UIHotbarItem> hotbarItems;
    public int selectedHotbarItem = -1;

    // Inventory Screen
    public Canvas inventoryCanvas;
    public Transform inventoryContent;

    // Crafting Screen
    public Canvas craftingCanvas;
    public Transform craftingContent;
    private bool craftingInitialSet = false;

    // Life Chest Screen
    public Canvas lifeChestCanvas;
    public Text lifeChestSelectedText;
    public int lifeChestSelectedID;
    public Transform lifeChestInventoryContent;
    public Transform lifeChestContainerContent;
    public Button lifeChestButtonChestToInventory;
    public Button lifeChestButtonInventoryToChest;
    public InputField lifeChestAmount;
    public LifeChest openedLifeChest;

    // Main Menu
    public Canvas mainMenuCanvas;
    public Button mainMenuPlayButton;
    public Button mainMenuCreditsButton;
    public Button mainMenuQuitButton;

    // Lose Menu
    public Canvas loseCanvas;
    public Button loseMainMenuButton;
    public Text loseText;

    // Pause Menu
    public Canvas pauseCanvas;
    public Button pauseResumeButton;
    public Button pauseQuitButton;

    // Credits Menu
    public Canvas creditsCanvas;

    private void Start()
    {
        Debug.Log("UI Controller start!");
        menuMap.Add(Menu.INVENTORY, inventoryCanvas);
        menuMap.Add(Menu.CRAFTING, craftingCanvas);
        menuMap.Add(Menu.LIFE_CHEST, lifeChestCanvas);
        menuMap.Add(Menu.MAIN, mainMenuCanvas);
        menuMap.Add(Menu.LOSE, loseCanvas);
        menuMap.Add(Menu.PAUSE, pauseCanvas);
        menuMap.Add(Menu.CREDITS, creditsCanvas);

        mainMenuPlayButton.onClick.AddListener(MainMenuPlay);
        mainMenuCreditsButton.onClick.AddListener(MainMenuCredits);
        mainMenuQuitButton.onClick.AddListener(MainMenuQuit);

        lifeChestButtonChestToInventory.onClick.AddListener(LifeChestToInventory);
        lifeChestButtonInventoryToChest.onClick.AddListener(InventoryToLifeChest);
        lifeChestAmount.onValueChanged.AddListener(InputLifeChestAmountChange);

        loseMainMenuButton.onClick.AddListener(ReloadGame);

        pauseResumeButton.onClick.AddListener(PauseMenuResume);
        pauseQuitButton.onClick.AddListener(MainMenuQuit); // TODO: Make custom method for pause menu here.        

        lifeChestSelectedText.text = "No Item Selected";
        lifeChestSelectedID = -1;

        hotbarItems = new List<UIHotbarItem>();
        for(int i = 0; i < 9; i++)
        {
            UIHotbarItem currHotbarEntry = Instantiate(Resources.Load<GameObject>("UI/HotbarItem"), hotbar).GetComponent<UIHotbarItem>();
            currHotbarEntry.SetItem(null);
            currHotbarEntry.SetNumber(i + 1);
            hotbarItems.Add(currHotbarEntry);
        }
    }

    private void Update()
    {
        if (GameController._GameController.gameState == GameState.GAME)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                ToggleMenu(Menu.INVENTORY);
            }

            if (Input.GetKeyDown(KeyCode.C))
            {
                ToggleMenu(Menu.CRAFTING);
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (currentMenu == Menu.NONE)
                {
                    ToggleMenu(Menu.PAUSE);
                }
                else
                {
                    SetMenu(Menu.NONE);
                }
            }
        }
    }

    public void ShowHUD(bool show)
    {
        hudCanvas.enabled = show;
    }

    private void ToggleMenu(Menu menu)
    {
        if (currentMenu == menu)
        {
            SetMenu(Menu.NONE);
        }
        else
        {
            SetMenu(menu);
        }
    }

    public void RefreshMenu(Menu menu, bool force = false)
    {
        if (currentMenu != menu && !force)
        {
            return;
        }

        SortedDictionary<int, int> inventory = GameController._GameController.GetInventory();
        switch (menu)
        {
            case Menu.INVENTORY:
                foreach (Transform curr in inventoryContent) // TODO: Save these to dictionary instead of doing this.
                {
                    Destroy(curr.gameObject);
                }

                foreach (KeyValuePair<int, int> curr in inventory)
                {
                    if (curr.Value <= 0)
                    {
                        continue;
                    }

                    Transform currMenuEntry = Instantiate(Resources.Load<GameObject>("UI/InventoryMenuItem"), inventoryContent).transform;
                    currMenuEntry.GetComponent<UIInventoryItem>().SetItem(ItemDB.ITEMS[curr.Key]);
                }
                break;
            case Menu.LIFE_CHEST:
                // Player Inventory
                foreach (Transform curr in lifeChestInventoryContent) // TODO: Save these to dictionary instead of doing this.
                {
                    Destroy(curr.gameObject);
                }

                foreach (KeyValuePair<int, int> curr in inventory)
                {
                    if (curr.Value <= 0)
                    {
                        continue;
                    }

                    Transform currMenuEntry = Instantiate(Resources.Load<GameObject>("UI/LifeChestInventoryMenuItem"), lifeChestInventoryContent).transform;
                    currMenuEntry.GetComponent<UILifeChestInventoryItem>().SetItem(ItemDB.ITEMS[curr.Key]);
                    currMenuEntry.GetComponent<UILifeChestInventoryItem>().SetQuantity((int)GameController._GameController.GetInventoryItem(curr.Key));
                }

                // Life Chest Inventory
                foreach (Transform curr in lifeChestContainerContent) // TODO: Save these to dictionary instead of doing this.
                {
                    Destroy(curr.gameObject);
                }

                foreach (KeyValuePair<int, int> curr in openedLifeChest.GetInventory())
                {
                    if (curr.Value <= 0)
                    {
                        continue;
                    }

                    Transform currMenuEntry = Instantiate(Resources.Load<GameObject>("UI/LifeChestInventoryMenuItem"), lifeChestContainerContent).transform;
                    currMenuEntry.GetComponent<UILifeChestInventoryItem>().SetItem(ItemDB.ITEMS[curr.Key]);
                    currMenuEntry.GetComponent<UILifeChestInventoryItem>().SetQuantity((int)openedLifeChest.GetInventoryItem(curr.Key));
                }

                ValidateLifeChest();
                break;
            case Menu.CRAFTING:
                if (!craftingInitialSet) // Crafting menu should only need to be updated once.
                {
                    foreach (KeyValuePair<int, Item> curr in ItemDB.ITEMS)
                    {
                        if (curr.Value.recipe != null) // Check if craftable.
                        {
                            Transform currCraftingEntry = Instantiate(Resources.Load<GameObject>("UI/CraftingMenuItem"), craftingContent).transform;
                            currCraftingEntry.GetComponent<UICraftingItem>().SetItem(curr.Value);
                        }
                    }
                    craftingInitialSet = true;
                }

                foreach (Transform curr in craftingContent)
                {
                    UICraftingItem currEntry = curr.GetComponent<UICraftingItem>();
                    currEntry.UpdateInventoryCounts();
                }
                break;
            case Menu.LOSE:
                if (GameController._GameController.GetPlayerStat(Stat.PLAYER_HEALTH) <= 0)
                {
                    loseText.text = "Defeat! You have died!";
                }
                else if (GameController._GameController.GetPlayerStat(Stat.WALL_HEALTH) <= 0)
                {
                    loseText.text = "Defeat! Your castle wall has fallen!";
                }
                else
                {
                    loseText.text = "I'm not sure why you lost. Tell a dev.";
                }
                break;
        }
    }

    public void SetMenu(Menu menu)
    {
        currentMenu = menu;

        FPSController.enabled = (currentMenu == Menu.NONE);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        RefreshMenu(menu);

        foreach (KeyValuePair<Menu, Canvas> curr in menuMap)
        {
            if (curr.Value != null)
            {
                if (curr.Key != menu)
                {
                    curr.Value.enabled = false;
                    curr.Value.gameObject.SetActive(false);
                }
                else
                {
                    curr.Value.gameObject.SetActive(true);
                    curr.Value.enabled = true;
                }
            }
        }
    }

    public void SetOpenedLifeChest(LifeChest lifeChest)
    {
        openedLifeChest = lifeChest;
    }

    public void LifeChestSelectItem(Item item)
    {
        lifeChestSelectedID = item.id;
        lifeChestSelectedText.text = item.name;
        ValidateLifeChest();
    }

    public void LifeChestToInventory()
    {
        LifeChestTransfer(false);
    }

    public void InventoryToLifeChest()
    {
        LifeChestTransfer(true);
    }

    public void LifeChestTransfer(bool inventoryToChest)
    {
        try
        {
            if (lifeChestSelectedID < 0)
            {
                Debug.LogWarning("No item selected by user.");
                return;
            }

            int amount = Int32.Parse(lifeChestAmount.text); //TODO: Do some subtraction to move the proper amount.
            if (inventoryToChest)
            {
                GameController._GameController.AddToInventoryItem(lifeChestSelectedID, -amount);
                openedLifeChest.AddToInventoryItem(lifeChestSelectedID, amount);
                Debug.Log($"Moving {amount} of {ItemDB.ITEMS[lifeChestSelectedID].name} from inventory to life chest.");
            }
            else
            {
                GameController._GameController.AddToInventoryItem(lifeChestSelectedID, amount);
                openedLifeChest.AddToInventoryItem(lifeChestSelectedID, -amount);
                Debug.Log($"Moving {amount} of {ItemDB.ITEMS[lifeChestSelectedID].name} from life chest to inventory.");
            }
        }
        catch (FormatException)
        {
            Debug.LogError("Could not convert user input to life chest menu.");
        }
    }

    private void InputLifeChestAmountChange(String value)
    {
        ValidateLifeChest();
    }

    public void ValidateLifeChest()
    {
        //TODO: Chest capacity could be validated here?

        int amount = Int32.Parse(lifeChestAmount.text);

        // Validate Inventory to Chest
        lifeChestButtonInventoryToChest.interactable = !(GameController._GameController.GetInventoryItem(lifeChestSelectedID) - amount < 0);

        // Validate Chest to Inventory
        lifeChestButtonChestToInventory.interactable = !(openedLifeChest.GetInventoryItem(lifeChestSelectedID) - amount < 0);
    }

    public List<UIHotbarItem> GetUIHotbarItems()
    {
        return hotbarItems;
    }

    public int GetMaxedHotbarIndex()
    {
        int lastGood = -1;
        
        for(int i = 0; i < hotbarItems.Count; i++)
        {
            if(hotbarItems[i].GetItem() != null)
            {
                lastGood = i;
            }
        }

        return lastGood;
    }

    public void MainMenuPlay()
    {
        Debug.Log("Menu play!");

        if (File.Exists(Path.Combine(Application.persistentDataPath, "save.dat")))
        {
            Debug.Log("Save file found. Loading.");
            GameController._GameController.Load();
        }
        else
        {
            Debug.Log("No save file found. Starting new game.");
        }

        GameController._GameController.SetGameState(GameState.GAME);
    }

    public void MainMenuCredits()
    {
        SetMenu(Menu.CREDITS);
    }

    public void MainMenuQuit()
    {
        if (Application.isEditor)
        {
            Debug.LogError("Can not exit from editor run.");
            return;
        }

        Application.Quit();
    }

    public void PauseMenuResume()
    {
        SetMenu(Menu.NONE);
    }

    public void ReloadGame()
    {
        SceneManager.LoadScene("Main", LoadSceneMode.Single);
    }
}

public enum Menu
{
    NONE, INVENTORY, CRAFTING, LIFE_CHEST, MAIN, LOSE, PAUSE, CREDITS
}