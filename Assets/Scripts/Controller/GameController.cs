﻿using static BalanceDB;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.NetworkInformation;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.Audio;
using System.Linq;

public class GameController : MonoBehaviour
{
    public static GameController _GameController;
    public GameState gameState;
    public AnimationCurve difficultyCurve;
    public AnimationCurve testDifficultyCurve;
    public AnimationCurve testDifficultyCurve2;
    public float currDifficulty;
    public int gameTime = 0;
    private SortedDictionary<int, int> playerInventory = new SortedDictionary<int, int>();
    private List<Item> heldHotbarItems;
    public Transform player;
    public Renderer backingPlaneRenderer;
    public Transform enemyPortal;
    public Transform enemyTarget;
    public Transform borderInvisiwalls;
    public List<Transform> treePads;
    public List<Transform> chestPads;
    private Dictionary<Stat, float> playerStats;
    public bool debugMode;
    public RaycastHit targetPointCenter;
    public Transform hitSphere;
    public UIController _UIController;

    public Transform viewModelHolder;
    public List<Enemy> enemies;
    public List<Tree> trees;
    public List<LifeChest> lifeChests;
    private Item heldItem;
    private Transform viewModel;
    private Transform towerGhost;
    public LayerMask placingMask;
    private RaycastHit placeHit;
    private float placeMaxDistance = 0.0f;
    private float placeMaxPadding = 0.25f;
    private bool placeHitDetect;

    public Camera mainMenuCamera;
    public Camera playerCamera;

    private AudioSource musicSource;
    public AudioMixerGroup battleMusicMixer;
    public AudioClip battleMusicClip;
    public AudioClip caveMusicClip;
    public AudioClip loseMusicClip;

    public bool isMine = false;
    public bool godMode = false;

    private List<SpawnFacet> spawnFacets;

    private bool inMines = false;

    private void Awake()
    {
        //placingMask = 1 << 8;
        //placingMask = ~placingMask;

        treePads = new List<Transform>();
        playerStats = new Dictionary<Stat, float>();
        if (_GameController == null)
        {
            _GameController = this;
        }
        else
        {
            Destroy(gameObject);
        }

        #region Debug
        HashSet<string> addresses = new HashSet<string>();

        IPGlobalProperties computerProperties = IPGlobalProperties.GetIPGlobalProperties();
        NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
        foreach (NetworkInterface adapter in nics)
        {
            string mac = adapter.GetPhysicalAddress().ToString();
            addresses.Add(mac);
        }

        if (addresses.Contains("382C4AB2B354"))
        {
            isMine = true;
            Debug.Log("Is mine!");
        }
        #endregion
    }

    private void Start()
    {
        Debug.Log("Game loaded.");
        //debugMode = !debugMode;

        backingPlaneRenderer.enabled = false;
        borderInvisiwalls.gameObject.SetActive(true);

        GenerateTrees();
        hitSphere = GameObject.Find("HitSphere").transform;

        musicSource = gameObject.AddComponent<AudioSource>();
        musicSource.outputAudioMixerGroup = battleMusicMixer;
        musicSource.clip = battleMusicClip;

        SetGameState(GameState.MAIN_MENU);
        SetHeldItem(null);

        SetupSpawnFacets();
    }

    private void SetupSpawnFacets()
    {
        spawnFacets = new List<SpawnFacet>();
        spawnFacets.Add(new SpawnFacet(EnemyType.SPINNER, EnemyColor.PURPLE, 10, 10, 0));
        spawnFacets.Add(new SpawnFacet(EnemyType.SPINNER, EnemyColor.BLUE, 20, 10, 0));
        spawnFacets.Add(new SpawnFacet(EnemyType.SPINNER, EnemyColor.YELLOW, 47, 20, 0));
        spawnFacets.Add(new SpawnFacet(EnemyType.SPINNER, EnemyColor.ORANGE, 72, 12, 0));
        spawnFacets.Add(new SpawnFacet(EnemyType.SPINNER, EnemyColor.RED, 90, 10, 0));
    }

    private void Update()
    {
        #region Debug
        if (Input.GetKey(KeyCode.X) && Input.GetKeyDown(KeyCode.D))
        {

            Debug.Log("Debug mode enabled!");
            debugMode = !debugMode;
        }

        if (debugMode)
        {
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.L))
            {
                System.Diagnostics.Process.Start(Application.persistentDataPath);
            }
        }
        #endregion

        switch (gameState)
        {
            case GameState.GAME:
                Physics.Raycast(playerCamera.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0)), out targetPointCenter);
                hitSphere.position = targetPointCenter.point;

                if (debugMode)
                {
                    if (Input.GetKeyDown(KeyCode.T))
                    {
                        SpawnEnemy();
                    }

                    if (Input.GetKeyDown(KeyCode.F3))
                    {
                        backingPlaneRenderer.enabled = !backingPlaneRenderer.enabled;
                    }

                    if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.G))
                    {
                        godMode = !godMode;
                    }
                }

                bool currentlyInMines = (player.transform.position.y < -1);
                if (inMines != currentlyInMines)
                {
                    SetInMines(currentlyInMines);
                }

                if (Input.GetKeyDown(KeyCode.Alpha0))
                {
                    SetHeldItem(null);
                }

                CheckHotbarInput();

                if (Input.GetKeyDown(KeyCode.K))
                {
                    SetPlayerStat(Stat.PLAYER_HEALTH, 0);
                }

                UpdateGhostPlacement();
                break;
        }
    }

    private void CheckHotbarInput()
    {
        List<System.Boolean> hotbarKeys = new List<System.Boolean>();
        hotbarKeys.Add(Input.GetKeyDown(KeyCode.Alpha1));
        hotbarKeys.Add(Input.GetKeyDown(KeyCode.Alpha2));
        hotbarKeys.Add(Input.GetKeyDown(KeyCode.Alpha3));
        hotbarKeys.Add(Input.GetKeyDown(KeyCode.Alpha4));
        hotbarKeys.Add(Input.GetKeyDown(KeyCode.Alpha5));
        hotbarKeys.Add(Input.GetKeyDown(KeyCode.Alpha6));
        hotbarKeys.Add(Input.GetKeyDown(KeyCode.Alpha7));
        hotbarKeys.Add(Input.GetKeyDown(KeyCode.Alpha8));
        hotbarKeys.Add(Input.GetKeyDown(KeyCode.Alpha9));
        
        for(int i = 0; i < hotbarKeys.Count; i++)
        {
            if(hotbarKeys[i])
            {
                try
                {
                    SetHeldItem(heldHotbarItems[i]);
                }
                catch
                {
                    Debug.LogWarning("Couldn't set hotbar item.");
                }
            }
        }

        List<UIHotbarItem> hotbarItems = _UIController.GetUIHotbarItems();

        if(_UIController.GetMaxedHotbarIndex() > 0)
        {
            if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                int nextNum = _UIController.selectedHotbarItem + 1;
                if (hotbarItems.Count > nextNum)
                {
                    SetHeldItem(hotbarItems[nextNum].GetItem());
                }
                else
                {
                    SetHeldItem(hotbarItems[0].GetItem());
                }
            }

            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                int nextNum = _UIController.selectedHotbarItem - 1;
                if (nextNum >= 0)
                {
                    SetHeldItem(hotbarItems[nextNum].GetItem());
                }
                else
                {
                    Item loopedItem = hotbarItems[_UIController.GetMaxedHotbarIndex()].GetItem();
                    SetHeldItem(loopedItem);
                }
            }
        }
    }

    public void SetGameState(GameState gameState)
    {
        Debug.Log($"Set  game state: {gameState}");
        this.gameState = gameState;

        switch (gameState)
        {
            case GameState.MAIN_MENU:
                mainMenuCamera.enabled = true;
                playerCamera.enabled = false;
                player.gameObject.SetActive(false);
                _UIController.SetMenu(Menu.MAIN);
                _UIController.ShowHUD(false);
                break;
            case GameState.GAME:
                player.gameObject.SetActive(true);

                playerInventory.Add(0, 100);
                //playerInventory.Add(1, 300);
                //playerInventory.Add(2, 1000);

                SetPlayerStat(Stat.PLAYER_HEALTH, PLAYER_MAX_HEALTH);
                SetPlayerStat(Stat.WALL_HEALTH, WALL_MAX_HEALTH);

                Invoke("SpawnEnemy", 0.5f);
                //InvokeRepeating("PrintStats", 0f, 1.0f);
                InvokeRepeating("UpdateDifficulty", 0.0f, 1.0f);

                mainMenuCamera.enabled = false;
                playerCamera.enabled = true;
                _UIController.SetMenu(Menu.NONE);
                _UIController.ShowHUD(true);

                PlayMusic(battleMusicClip);
                break;
            case GameState.LOSE:
                _UIController.SetMenu(Menu.LOSE);
                _UIController.ShowHUD(false);

                PlayMusic(loseMusicClip);
                break;
        }
    }

    private void UpdateDifficulty()
    {
        gameTime++;
        //float adjustedTime = gameTime / 60.0f; // Run within one minute
        float minutes = 5.0f;
        float adjustedTime = gameTime / 60.0f / minutes; // Run within one hour
        currDifficulty = testDifficultyCurve2.Evaluate(adjustedTime);
        Debug.LogWarning($"Time: {gameTime} Adjusted time: {adjustedTime}s Difficulty: {currDifficulty}");

        UpdateSpawnFacets();
    }

    private void UpdateSpawnFacets()
    {
        foreach (SpawnFacet curr in spawnFacets)
        {
            curr.spawnProb = GetHillProbability(curr);

            if(curr.color == EnemyColor.RED && currDifficulty == 1) // Hack to make reds spawn at the end.
            {
                curr.spawnProb = 1;
            }
        }

        PrintSpawnFacets();
    }

    private void PrintSpawnFacets()
    {
        Debug.Log("|==============|");
        Debug.Log("|_Spawn Facets_|");
        Debug.Log("|==============|");
        foreach (SpawnFacet curr in spawnFacets)
        {
            Debug.Log($"Probability for {curr.color}: {curr.spawnProb}");
        }
        Debug.Log("==================================================");
    }

    public float GetHillProbability(SpawnFacet facet)
    {
        float _min = facet.centerPoint - facet.spawnBreadth;
        float _max = facet.centerPoint + facet.spawnBreadth;

        float normCenter = facet.centerPoint - _min;


        float normTime = (currDifficulty * 100) - _min;
        float prob = normTime / normCenter;

        if (prob > 1 && prob <= 2)
        {
            float rem = prob - 1;
            prob = 1 - rem;
        }
        else if (prob > 2)
        {
            prob = 0;
        }

        return Mathf.Clamp01(prob);
    }

    private void UpdateGhostPlacement()
    {
        if (towerGhost != null && heldItem != null)
        {
            if ((targetPointCenter.point - player.position).sqrMagnitude < 200)
            {
                towerGhost.position = _GameController.targetPointCenter.point;
            }
            else
            {
                if (heldItem.id >= 100 && heldItem.id <= 199)
                {
                    foreach (Renderer curr in towerGhost.GetComponentInChildren<Tower>().renderers) // TODO: Extract to method
                    {
                        curr.material.color = new Color(1.0f, 0.0f, 0.0f, 0.0f);
                    }
                }

                if (heldItem.id == 200)
                {
                    //Debug.Log("Out of range!");
                    //Debug.LogError($"Renderer count: {towerGhost.GetComponentInChildren<LifeChest>().renderers.Count}");
                    foreach (Renderer curr in towerGhost.GetComponentInChildren<LifeChest>().renderers) // TODO: Extract to method
                    {
                        curr.material.color = new Color(1.0f, 0.0f, 0.0f, 0.0f);
                    }
                }

                return;
            }

            bool isValid = false;
            if (heldItem != null)
            {
                if (heldItem.id >= 100 && heldItem.id <= 199) // Limit to Towers.
                {
                    foreach (Transform curr in treePads) // TODO: Make spawn pads instead of treepads.
                    {
                        if (curr.GetComponent<Collider>().bounds.Contains(targetPointCenter.point))
                        {
                            isValid = true;
                            break;
                        }
                    }
                }
                else if (heldItem.id == 200)
                {
                    foreach (Transform curr in chestPads)
                    {
                        if (curr.GetComponent<Collider>().bounds.Contains(targetPointCenter.point))
                        {
                            isValid = true;
                            break;
                        }
                    }
                }
            }

            if (isValid) // Check possible collisions.
            {
                foreach (Collider currPlaceCollider in towerGhost.GetComponentsInChildren<Collider>())
                {
                    Vector3 currColliderPosition = currPlaceCollider.bounds.center;
                    Vector3 currColliderSize = currPlaceCollider.bounds.size + (new Vector3(1, 1, 1) * placeMaxPadding);
                    placeHitDetect = Physics.CheckBox(currColliderPosition, currColliderSize, towerGhost.rotation, placingMask);

                    if (placeHitDetect && !currPlaceCollider.isTrigger)
                    {
                        isValid = false;
                        break;
                    }
                }
            }

            if (heldItem != null)
            {
                if (heldItem.id >= 100 && heldItem.id <= 199)
                {
                    foreach (Renderer curr in towerGhost.GetComponentInChildren<Tower>().renderers)
                    {
                        curr.material.color = isValid ? new Color(1.0f, 1.0f, 1.0f, 0.5f) : new Color(1.0f, 0.0f, 0.0f, 0.5f);
                    }
                }

                if (heldItem.id == 200)
                {
                    foreach (Renderer curr in towerGhost.GetComponentInChildren<LifeChest>().renderers)
                    {
                        curr.material.color = isValid ? new Color(1.0f, 1.0f, 1.0f, 0.5f) : new Color(1.0f, 0.0f, 0.0f, 0.5f);
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.Mouse0) && (_UIController.currentMenu == Menu.NONE))
            {
                if (isValid)
                {
                    if (heldItem.id >= 100 && heldItem.id <= 199)
                    {
                        towerGhost.GetComponentInChildren<Tower>().SetState(PlaceState.ACTIVE);

                    }

                    if (heldItem.id == 200)
                    {
                        towerGhost.GetComponentInChildren<LifeChest>().SetState(PlaceState.ACTIVE);
                    }

                    towerGhost = null;
                    AddToInventoryItem(heldItem.id, -1);
                    SetHeldItem(heldItem); // Give player their item back.
                }
            }
        }
    }

    public void SetHeldItem(Item item)
    {
        heldItem = item;
        Debug.Log($"Held item {((item == null) ? "cleared" : $"set to {heldItem.name}")}");

        if (viewModel)
        {
            Destroy(viewModel.gameObject);
        }

        if (towerGhost)
        {
            Destroy(towerGhost.gameObject);
        }

        foreach(UIHotbarItem curr in _UIController.GetUIHotbarItems())
        {
            if(item == null)
            {
                curr.SetItemHighlighted(false);
            }
            else
            {
                curr.SetItemHighlighted((curr.GetItem() == item));
            }
        }

        if (heldItem != null)
        {
            if (GetInventoryItem(item.id) <= 0) // Make sure player has item in order to hold it.
            {
                SetHeldItem(null);
                return;
            }

            if (heldItem.id == 100)
            {
                viewModel = Instantiate(Resources.Load<GameObject>("ViewModels/View Model Tower Basic"), viewModelHolder).transform;
                towerGhost = Instantiate(Resources.Load<GameObject>("Towers/Tower Basic"), _GameController.targetPointCenter.point, Quaternion.identity).transform;
                towerGhost.GetComponentInChildren<Tower>().SetState(PlaceState.PLACING);
            }
            if (heldItem.id == 101)
            {
                viewModel = Instantiate(Resources.Load<GameObject>("ViewModels/View Model Tower Snow"), viewModelHolder).transform;
                towerGhost = Instantiate(Resources.Load<GameObject>("Towers/Tower Snow"), _GameController.targetPointCenter.point, Quaternion.identity).transform;
                towerGhost.GetComponentInChildren<Tower>().SetState(PlaceState.PLACING);
            }
            if (heldItem.id == 200)
            {
                viewModel = Instantiate(Resources.Load<GameObject>("ViewModels/View Model Life Chest"), viewModelHolder).transform;
                towerGhost = Instantiate(Resources.Load<GameObject>("LifeChest"), _GameController.targetPointCenter.point, Quaternion.identity).transform;
                towerGhost.GetComponentInChildren<LifeChest>().SetState(PlaceState.PLACING);
            }
        }
        else
        {
            _UIController.selectedHotbarItem = -1;
        }
    }

    public void SpawnEnemy()
    {
        GameObject enemy = Instantiate(Resources.Load<GameObject>("Enemies/Spinner"), enemyPortal.position, Quaternion.identity);
        EnemyColor enemyColor = GetEnemyColorToSpawn();
        enemy.GetComponent<Enemy>().SetEnemyColor(enemyColor);

        //Debug.Log($"Spawning a {enemyColor}");

        float spawnTime = ((1 - currDifficulty) * 2.75f) + 0.25f;
        spawnTime = Mathf.Clamp(spawnTime, 0.75f, 3);
        //Debug.Log($"Spawn time: {spawnTime}");
        Invoke("SpawnEnemy", spawnTime);
    }

    private EnemyColor GetEnemyColorToSpawn()
    {
        EnemyColor color = EnemyColor.PURPLE;

        List<SpawnFacet> currFacets = new List<SpawnFacet>(spawnFacets);
        currFacets.Sort(delegate (SpawnFacet c1, SpawnFacet c2) { return c1.spawnProb.CompareTo(c2.spawnProb); });

        for (int i = 0; i < currFacets.Count; i++)
        {
            SpawnFacet curr = currFacets[i];

            if (curr.spawnProb == 0)
            {
                continue;
            }

            if ((Random.value < curr.spawnProb) || (i == (currFacets.Count - 1)))
            {
                color = curr.color;
                break;
            }
        }

        return color;
    }

    public void GenerateTrees()
    {
        Debug.Log("Generate trees!");
        foreach (Transform curr in treePads)
        {
            Bounds currBounds = curr.GetComponent<Collider>().bounds;
            float currArea = currBounds.size.x * currBounds.size.y;
            int currTreeCount = (int)Mathf.Ceil(currArea * TREES_PER_UNIT);
            float minX = currBounds.min.x;
            float maxX = currBounds.max.x;
            float minZ = currBounds.min.z;
            float maxZ = currBounds.max.z;

            for (int i = 0; i < currTreeCount; i++)
            {
                Vector3 randomPos = new Vector3(Random.Range(minX, maxX), 0, Random.Range(minZ, maxZ));
                GameObject obj = Instantiate(Resources.Load<GameObject>("Tree"), randomPos, Quaternion.identity);
                Tree currTree = obj.GetComponent<Tree>();
                currTree.originalPosition = randomPos;
                currTree.RandomizeHeight();
            }
            //Debug.Log($"{curr.name}\n   Area: {currArea}\n  Tree Count: {currTreeCount}\n   Xb: {minX}, {maxX}\n    Zb: {minZ}, {maxZ}");
        }
    }

    public void RemoveTrees()
    {
        Debug.Log("Removing all trees.");
        foreach (Tree curr in trees)
        {
            Destroy(curr.gameObject);
        }
    }

    public void SetInMines(bool inMines)
    {
        this.inMines = inMines;
        RenderSettings.fog = inMines;
        if (!inMines)
        {
            float time = musicSource.time;
            PlayMusic(battleMusicClip);
            musicSource.time = time;
        }
        else
        {
            float time = musicSource.time;
            PlayMusic(caveMusicClip);
            musicSource.time = time;
        }
    }

    public void AddToInventoryItem(int id, int amount)
    {
        if (!playerInventory.ContainsKey(id))
        {
            playerInventory.Add(id, amount);
        }
        else
        {
            playerInventory[id] += amount;
        }

        RefreshHUD();
        _UIController.RefreshMenu(Menu.INVENTORY);
        _UIController.RefreshMenu(Menu.LIFE_CHEST);
        _UIController.RefreshMenu(Menu.CRAFTING); // To update descriptions.

        UpdateHotbar();
    }

    public void UpdateHotbar()
    {
        heldHotbarItems = new List<Item>();
        foreach(KeyValuePair<int, int> curr in playerInventory)
        {
            if(ItemDB.ITEMS[curr.Key].type != ItemType.COLLECTABLE && curr.Value > 0)
            {
                heldHotbarItems.Add(ItemDB.ITEMS[curr.Key]);
            }
        }

        List<UIHotbarItem> hotbarItems = _UIController.GetUIHotbarItems();
        for (int i = 0; i < hotbarItems.Count; i++)
        {
            UIHotbarItem curr = hotbarItems[i];
            curr.SetItem(null);
            if(i < heldHotbarItems.Count && heldHotbarItems[i] != null)
            {
                curr.SetItem(heldHotbarItems[i]);
            }
        }
    }

    public float GetInventoryItem(int id)
    {
        return (playerInventory.ContainsKey(id)) ? playerInventory[id] : 0.0f;
    }

    public float GetInventoryItem(Item item)
    {
        return GetInventoryItem(item.id);
    }

    public SortedDictionary<int, int> GetInventory()
    {
        return playerInventory;
    }

    public bool ProcessRecipe(Item item)
    {
        bool runChecks = true;
        if(debugMode && Input.GetKey(KeyCode.LeftControl))
        {
            runChecks = false;
        }

        if (runChecks)
        {
            foreach (RecipeFacet facet in item.recipe.facets) // Check if player has enough of each ingredient.
            {
                if ((GetInventoryItem(facet.itemID) < facet.count))
                {
                    Debug.Log($"Could not craft {item.name}, not enough {ItemDB.ITEMS[facet.itemID].name}");
                    return false;
                }
            }

            foreach (RecipeFacet facet in item.recipe.facets) // Remove each ingredient from inventory.
            {
                AddToInventoryItem(facet.itemID, -facet.count);
            }
        }

        AddToInventoryItem(item.id, 1); // Add crafted item to inventory.
        Debug.Log($"Crafted {item.name}");
        return true;
    }

    public float GetPlayerStat(Stat stat)
    {
        return (playerStats.ContainsKey(stat)) ? playerStats[stat] : 0.0f;
    }

    public void SetPlayerStat(Stat stat, float value)
    {
        if (debugMode)
        {
            Debug.Log($"Setting stat {stat} to {value}");
        }

        playerStats[stat] = value;

        if (stat == Stat.PLAYER_HEALTH || stat == Stat.WALL_HEALTH)
        {
            if (playerStats[stat] <= 0)
            {
                if (gameState == GameState.GAME)
                {
                    Debug.Log("Player has died!");
                    SetGameState(GameState.LOSE);
                    _UIController.RefreshMenu(Menu.LOSE);
                }
            }
        }

        RefreshHUD();
    }

    public void AddToPlayerStat(Stat stat, float amount)
    {
        SetPlayerStat(stat, GetPlayerStat(stat) + amount);
    }

    public static void SetObjectState(Transform _Transform, PlaceState state)
    {
        Collider[] colliders = _Transform.gameObject.GetComponentsInChildren<Collider>();
        Renderer[] renderers = _Transform.gameObject.GetComponentsInChildren<Renderer>();

        if (colliders.Length == 0)
        {
            colliders = _Transform.gameObject.GetComponentsInChildren<Collider>();
        }

        foreach (Renderer curr in renderers)
        {
            Debug.Log("SETTING RENDERER");
            if (state != PlaceState.ACTIVE)
            {
                curr.material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
            }
            else
            {
                curr.material.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            }
        }

        Debug.Log("Renderers size: " + renderers.Length);
        Debug.Log("Colliders size: " + colliders.Length);
        foreach (Collider curr in colliders)
        {
            curr.enabled = state == PlaceState.ACTIVE;
            Debug.Log("Setting trigger to " + curr.isTrigger);
        }

        if (state == PlaceState.PLACING)
        {
            SetLayerRecursively(_Transform.gameObject, 2); // Try transform.parent if this doesnt work
        }
        else
        {
            SetLayerRecursively(_Transform.gameObject, 0);
        }
    }

    public static void SetLayerRecursively(GameObject obj, int newLayer)
    {
        if (null == obj)
        {
            return;
        }

        int finalLayer = newLayer;          // Workaround to keep snow collider on the ignore raycast layer.
        if (obj.tag == "snow_collider") 
        {
            finalLayer = 2;
        }

        obj.layer = finalLayer;

        foreach (Transform child in obj.transform)
        {
            if (null == child)
            {
                continue;
            }
            SetLayerRecursively(child.gameObject, newLayer);
        }
    }

    public void PlayMusic(AudioClip clip)
    {
        musicSource.clip = clip;
        musicSource.loop = (clip == battleMusicClip);
        if(clip == loseMusicClip)
        {
            musicSource.time = 0;
        }
        musicSource.Play();
    }

    public void RefreshHUD()
    {
        _UIController.PlayerHealthText.text = GetPlayerStat(Stat.PLAYER_HEALTH).ToString();
        _UIController.WallHealthText.text = GetPlayerStat(Stat.WALL_HEALTH).ToString();
        _UIController.LifeDustInvText.text = PrettyNumberFormat(GetInventoryItem(0));
        _UIController.WoodInvText.text = PrettyNumberFormat(GetInventoryItem(1));
        _UIController.StoneInvText.text = PrettyNumberFormat(GetInventoryItem(2));
    }

    public void Save()
    {
        string path = Path.Combine(Application.persistentDataPath, "save.dat");
        try
        {
            Debug.Log("Saving game to: " + path);

            BinaryFormatter _BinaryFormatter = new BinaryFormatter();
            FileStream _FileStream = File.Create(path);

            SaveData _SaveData = new SaveData();

            foreach (LifeChest curr in lifeChests)
            {
                LifeChestSaveData currLifeChestData = new LifeChestSaveData();
                currLifeChestData.posX = curr.transform.position.x;
                currLifeChestData.posY = curr.transform.position.y;
                currLifeChestData.posZ = curr.transform.position.z;
                currLifeChestData.inventory = curr.GetInventory();

                _SaveData.lifeChests.Add(currLifeChestData);
            }

            _BinaryFormatter.Serialize(_FileStream, _SaveData);
            _FileStream.Close();

            Debug.Log("Save game successful!");
        }
        catch (System.Exception ex)
        {
            Debug.LogError($"Could not save game!\n{ex.StackTrace}");
        }
    }

    public void Load()
    {
        string path = Path.Combine(Application.persistentDataPath, "save.dat");

        Debug.Log("Loading game from: " + path);
        if (File.Exists(path)) // If game save file is found...
        {
            try
            {
                BinaryFormatter _BinaryFormatter = new BinaryFormatter();
                FileStream _FileStream = File.Open(path, FileMode.Open);

                SaveData _SaveData = (SaveData)_BinaryFormatter.Deserialize(_FileStream);
                _FileStream.Close();

                foreach (LifeChestSaveData curr in _SaveData.lifeChests)
                {
                    Vector3 pos = new Vector3(curr.posX, curr.posY, curr.posZ);
                    GameObject obj = Instantiate(Resources.Load<GameObject>("LifeChest"), pos, Quaternion.identity);
                    LifeChest currChest = obj.GetComponent<LifeChest>();
                    currChest.chestInventory = curr.inventory;
                    currChest.SetState(PlaceState.ACTIVE);
                }

                Debug.Log("The game was sucessfully loaded.");
            }
            catch (System.Exception ex)
            {
                Debug.LogError($"The save file is corrupted. Load aborted.\n{ex.StackTrace}");
            }
        }
        else
        {
            Debug.LogError($"The game could not be loaded from the path {path}.");
        }
    }

    public void PrintStats()
    {
        if (!debugMode)
        {
            return;
        }

        foreach (KeyValuePair<Stat, float> curr in playerStats)
        {
            Debug.Log($"{curr.Key}: {curr.Value}");
        }

        Debug.Log($"Enemy Count: {enemies.Count}");
    }

    public static string PrettyNumberFormat(float num)
    {
        if (num < 1000)
        {
            return num.ToString();
        }
        else if (num < 1000000)
        {
            return $"{System.Math.Round((num / 1000), 2)}k";
        }
        else
        {
            return $"{System.Math.Round((num / 1000000), 2)}m";
        }
    }
}

public class SpawnFacet
{
    public EnemyType enemy;
    public EnemyColor color;
    public float centerPoint;
    public float spawnBreadth;
    public float spawnProb;


    public SpawnFacet(EnemyType enemy, EnemyColor color, float centerPoint, float spawnBreadth, float spawnProb)
    {
        this.enemy = enemy;
        this.color = color;
        this.centerPoint = centerPoint;
        this.spawnBreadth = spawnBreadth;
        this.spawnProb = spawnProb;
    }
}

public enum Stat
{
    PLAYER_HEALTH,
    WALL_HEALTH
}

public enum GameState
{
    MAIN_MENU, GAME, LOSE
}

public enum PlaceState
{
    PLACING,
    ACTIVE
}
