﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
    public float min = -6;
    public float max = 5;
    public float height = 0;
    public Vector3 originalPosition;

    Collider _Collider;

    private void Start()
    {
        _Collider = transform.Find("TreeTrunk").GetComponent<Collider>();
        GameController._GameController.trees.Add(this);
    }

    public void RandomizeHeight()
    {
        float selector = Random.value;
        float tmax = max;

        if (selector <= 0.05f)
        {
            tmax = max;
        }
        else if (selector <= 0.30f)
        {
            tmax = 4;
        }
        else if (selector <= 0.60)
        {
            tmax = 3;
        }
        else
        {
            tmax = 2;
        }

        height = Random.Range(min, tmax);
        gameObject.transform.position = originalPosition + new Vector3(0, height, 0);
    }

    public void Update()
    {
        if(GameController._GameController.gameState != GameState.GAME)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (_Collider.bounds.Contains(GameController._GameController.targetPointCenter.point))
            {
                if (Vector3.Distance(GameController._GameController.player.transform.position, _Collider.bounds.ClosestPoint(GameController._GameController.player.transform.position)) <= 2.75f)
                {
                    GameController._GameController.AddToInventoryItem(1, Mathf.CeilToInt(height + -min));
                    Destroy(gameObject);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.U))
        {
            RandomizeHeight();
        }
    }

    public void SetHeight(float height)
    {
        this.height = height;
    }
}
