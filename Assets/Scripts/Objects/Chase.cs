﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase : MonoBehaviour
{
    private Vector3 rotationChange = new Vector3(0, 1, 0); // Best range: 0 - 40

    void Start()
    {
        gameObject.SetActive(GameController._GameController.isMine && Application.isEditor);
    }

    void FixedUpdate()
    {
        transform.Rotate(rotationChange);
    }
}
