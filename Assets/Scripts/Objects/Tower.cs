﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public int towerType = 100;

    public PlaceState currentState;

    private float searchInterval = 0.2f;
    private float searchRange = 20.0f;
    private float shootInterval = 1.0f;

    private Enemy target = null;
    public Transform head;
    public List<Renderer> renderers = new List<Renderer>();
    public List<Collider> colliders = new List<Collider>();

    private void Start()
    {
        InvokeRepeating("SearchForNewTarget", 0.0f, searchInterval);
        InvokeRepeating("UpdateRotation", 0.0f, 0.01f);

        if(towerType == 100) // Only shoot if its a shootey tower.
        {
            InvokeRepeating("Shoot", 0.0f, shootInterval);
            renderers.AddRange(transform.GetComponentsInChildren<Renderer>());
            colliders.AddRange(transform.GetComponentsInChildren<Collider>());
        }        
        else if(towerType == 101) // For snow tower, the script is placed on a 1st level childs object, so we need to get the parents children.
        {
            renderers.AddRange(transform.parent.GetComponentsInChildren<Renderer>());
            colliders.AddRange(transform.parent.GetComponentsInChildren<Collider>());
        }

        Debug.Log($"Tower of type {ItemDB.ITEMS[towerType].name} has initialized at {transform.position}");

        //SetState(currentState);
    }

    private void SearchForNewTarget()
    {
        if (currentState != PlaceState.ACTIVE)
        {
            return;
        }

        if (target == null)
        {
            Enemy closestTarget = null;
            float closestDistSquared = Mathf.Infinity;
            foreach (Enemy curr in GameController._GameController.enemies)
            {
                Transform currTransform = curr.transform;
                Vector3 currDir = currTransform.position - transform.position;
                float currDist = currDir.sqrMagnitude;
                if (currDist < closestDistSquared && currDist < Mathf.Pow(searchRange, 2))
                {
                    closestDistSquared = currDir.sqrMagnitude;
                    closestTarget = curr;
                }
            }
            target = closestTarget;
        }
        else
        {
            if ((target.transform.position - transform.position).sqrMagnitude > Mathf.Pow(searchRange, 2))
            {
                target = null;
            }
        }
    }

    private void UpdateRotation()
    {
        if (currentState != PlaceState.ACTIVE)
        {
            return;
        }

        if (target)
        {
            Vector3 dir = target.transform.position - head.position;
            
            Quaternion qrot = Quaternion.LookRotation(dir);
            head.eulerAngles = new Vector3(270, qrot.eulerAngles.y, 0);
            Debug.DrawLine(head.position, target.transform.position, Color.green);
        }
    }

    private void Shoot()
    {
        if (!target || currentState != PlaceState.ACTIVE)
        {
            return;
        }

        GameObject projectile = Instantiate(Resources.Load<GameObject>("Projectiles/Tower/Projectile Basic"), head.position + new Vector3(0, 0, 0), head.rotation);
        //projectile.GetComponent<Projectile>().direction = head.forward;
        projectile.GetComponent<Projectile>().target = target.transform;
        projectile.GetComponent<Projectile>().damage = BalanceDB.BASIC_TOWER_DAMAGE;
    }

    public void SetState(PlaceState state)
    {
        currentState = state;
        switch (towerType)
        {
            case 100:
                GameController.SetObjectState(transform, state);
                break;
            case 101:
                GameController.SetObjectState(transform.parent, state);
                transform.parent.GetComponentInChildren<Light>().enabled = (state == PlaceState.ACTIVE);
                var systems = transform.parent.GetComponentsInChildren<ParticleSystem>();
                foreach (ParticleSystem curr in systems)
                {
                    if(state == PlaceState.ACTIVE)
                    {
                        curr.Play();
                    }
                    else
                    {
                        curr.Stop();
                    }
                }
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (currentState == PlaceState.ACTIVE)
        {
            if (towerType == 101)
            {
                Enemy enemy = other.transform.GetComponent<Enemy>();
                if (enemy)
                {
                    enemy.SetStatusEffect(EnemyStatusEffect.FROZEN);
                }
            }
        }
    }

    private void Update()
    {

    }
}

