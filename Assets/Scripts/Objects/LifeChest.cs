﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeChest : MonoBehaviour
{
    public Dictionary<int, int> chestInventory = new Dictionary<int, int>();

    private Collider _Collider;
    private PlaceState currentState;

    public List<Renderer> renderers = new List<Renderer>();
    public List<Collider> colliders = new List<Collider>();

    private void Start()
    {
        _Collider = GetComponent<Collider>();
        
        renderers.AddRange(gameObject.GetComponentsInChildren<Renderer>());
        colliders.AddRange(gameObject.GetComponentsInChildren<Collider>());
    }

    private void Update()
    {
        if(currentState == PlaceState.ACTIVE)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                if (_Collider.bounds.Contains(GameController._GameController.targetPointCenter.point))
                {
                    if (Vector3.Distance(GameController._GameController.player.transform.position, _Collider.bounds.ClosestPoint(GameController._GameController.player.transform.position)) <= 2.75f)
                    {
                        GameController._GameController._UIController.SetOpenedLifeChest(this);
                        GameController._GameController._UIController.SetMenu(Menu.LIFE_CHEST);
                    }
                }
            }
        }        
    }

    public void AddToInventoryItem(int id, int amount)
    {
        if (!chestInventory.ContainsKey(id))
        {
            chestInventory.Add(id, amount);
        }
        else
        {
            chestInventory[id] += amount;
        }

        GameController._GameController.RefreshHUD();

        GameController._GameController._UIController.RefreshMenu(Menu.INVENTORY);
        GameController._GameController._UIController.RefreshMenu(Menu.LIFE_CHEST);
        GameController._GameController._UIController.RefreshMenu(Menu.CRAFTING); // To update descriptions.

        GameController._GameController.Save(); // Save game after every chest modification.
    }

    public float GetInventoryItem(int id)
    {
        return (chestInventory.ContainsKey(id)) ? chestInventory[id] : 0.0f;
    }

    public float GetInventoryItem(Item item)
    {
        return GetInventoryItem(item.id);
    }

    public Dictionary<int, int> GetInventory()
    {
        return chestInventory;
    }

    public void SetState(PlaceState state)
    {
        currentState = state;
        GameController.SetObjectState(transform, state);

        if(state == PlaceState.ACTIVE)
        {
            GameController._GameController.lifeChests.Add(this);
        }

        transform.GetComponentInChildren<Light>().enabled = (state == PlaceState.ACTIVE);
        var systems = transform.GetComponentsInChildren<ParticleSystem>();
        foreach (ParticleSystem curr in systems)
        {
            if (state == PlaceState.ACTIVE)
            {
                curr.Play();
            }
            else
            {
                curr.Stop();
            }
        }
    }
}
