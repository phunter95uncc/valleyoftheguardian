﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreSpot : MonoBehaviour
{
    private float height;
    private Item item;
    private static Dictionary<int, float> oreProbabilities;
    private float startingProb = 0.38f;    // Probability that this ore will exist.
    private float minRespawnTime = 10.0f; // Minimum time after mining that new ore will appear here.
    private float maxRespawnTime = 30.0f; // Maximum time after mining that new ore will appear here.
    private int oreRemaining = 0;         // Current amount of ore remaining. 
    private int maxOre = 100;             // Maximum ore that is holdable.
    private bool isRespawning = false;
    public Collider _Collider;

    static OreSpot()
    {
        oreProbabilities = new Dictionary<int, float>();
        oreProbabilities.Add(2, 0.80f);
        oreProbabilities.Add(3, 0.20f);
    }

    private void Start()
    {
        if (!(Random.value < startingProb))
        {
            Destroy(gameObject);
        }       

        PickNewOre();
    }

    private void Update()
    {
        if (GameController._GameController.gameState != GameState.GAME)
        {
            return;
        }

        if (oreRemaining > 0 && Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (_Collider.bounds.Contains(GameController._GameController.targetPointCenter.point))
            {
                if (Vector3.Distance(GameController._GameController.player.transform.position, _Collider.bounds.ClosestPoint(GameController._GameController.player.transform.position)) <= 2.75f)
                {
                    int amount = Mathf.Min(Random.Range(1, 10), oreRemaining);
                    GameController._GameController.AddToInventoryItem(item.id, amount);
                    SetOreRemaining(oreRemaining - amount);
                }
            }
        }
    }

    public void PickNewOre()
    {
        foreach(KeyValuePair<int, float> curr in oreProbabilities)
        {
            if(Random.value < curr.Value)
            {
                SetItem(ItemDB.ITEMS[curr.Key]);
            }
        }

        if(item == null)
        {
            SetItem(ItemDB.ITEMS[2]);
        }

        SetOreRemaining(Random.Range(10, maxOre));
    }

    public void SetItem(Item item)
    {
        this.item = item;
        
        GetComponentInChildren<Renderer>().material.mainTexture = Resources.Load<Texture>($"World/OreTextures/{item.id}");
    }

    public void SetOreRemaining(int amount)
    {
        oreRemaining = amount;
        SetHeight(Mathf.Clamp01((oreRemaining / (float)maxOre) + 0.1f));

        if (amount <= 0)
        {
            if(amount < 0)
            {
                Debug.LogError("Ore remaning is negative!");
            }

            GetComponentInChildren<Renderer>().enabled = false;

            if (!isRespawning)
            {
                RequestRespawn();
            }            
        }
        else
        {
            GetComponentInChildren<Renderer>().enabled = true;
        }
    }

    public void RequestRespawn()
    {
        isRespawning = true;
        Invoke("Respawn", Random.Range(minRespawnTime, maxRespawnTime));
    }

    public void Respawn()
    {
        isRespawning = false;
        PickNewOre();        
    }

    public void SetHeight(float height)
    {
        this.height = height;
        transform.localScale = new Vector3(1, 1, height * 1);
    }
}
