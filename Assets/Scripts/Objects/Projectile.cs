﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Transform target;
    public Vector3 direction;
    private Rigidbody body;
    private Vector3 startPos;
    public float damage;

    void Start()
    {
        body = GetComponent<Rigidbody>();
        startPos = transform.position;
    }

    void Update()
    {
        //Debug.Log($"Forcing at {direction}");
        //body.AddForce(direction * 500);

        if(!target)
        {
            Destroy(gameObject);
            return;
        }

        float step = 20.0f * Time.deltaTime;        
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
    }

    void OnCollisionEnter(Collision collision)
    {
        Enemy otherEnemy = collision.other.transform.GetComponent<Enemy>();
        if (otherEnemy)
        {
            otherEnemy.TakeDamage(damage);
            Destroy(gameObject);
        }        
    }
}