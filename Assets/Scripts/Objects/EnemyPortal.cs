﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPortal : MonoBehaviour
{
    private Vector3 rotationChange = new Vector3(0, 0, 1);

    void FixedUpdate()
    {
        transform.Rotate(rotationChange);
    }
}
