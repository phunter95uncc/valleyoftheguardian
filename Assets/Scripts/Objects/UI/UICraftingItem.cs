﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICraftingItem : MonoBehaviour
{
    private Item item;
    public Button button;
    public Image picture;
    public Text title;
    public Text quantity;
    public Text description;

    public void SetItem(Item item)
    {
        this.item = item;
        title.text = item.name;
        quantity.text = $"";
        UpdateInventoryCounts(); // TODO: Redundent?

        picture.sprite = Resources.Load<Sprite>($"UI/UITextures/{item.id}");

        button.onClick.AddListener(ButtonClick);
    }

    public void UpdateInventoryCounts()
    {
        if(item == null) // I haven't been "created" yet
        {
            return;
        }

        Debug.Assert(description != null);
        Debug.Assert(description.text != null);
        Debug.Assert(item != null);
        Debug.Assert(item.description != null);
        description.text = item.description;

        foreach (RecipeFacet facet in item.recipe.facets)
        {
            string color = (GameController._GameController.GetInventoryItem(facet.itemID) >= facet.count) ? "black" : "red";
            description.text += $"\n    <color={color}>{ItemDB.ITEMS[facet.itemID].name}   x{facet.count}</color>";
        }
    }

    private void ButtonClick()
    {
        GameController._GameController.ProcessRecipe(item);
    }
}
