﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILifeChestInventoryItem : MonoBehaviour
{
    private Item item;
    public Button button;
    public Image picture;
    public Text title;
    public Text quantity;

    public void SetItem(Item item)
    {
        this.item = item;
        title.text = item.name;
        quantity.text = $"xnull";

        picture.sprite = Resources.Load<Sprite>($"UI/UITextures/{item.id}");

        button.onClick.AddListener(SelectItem);
    }

    public void SelectItem()
    {
        GameController._GameController._UIController.LifeChestSelectItem(item);
    }

    public void SetQuantity(int quantity)
    {
        this.quantity.text = $"x{quantity}";
    }
}
