﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInventoryItem : MonoBehaviour
{
    private Item item;
    public Button button;
    public Image picture;
    public Text title;
    public Text quantity;
    public Text description;

    public void SetItem(Item item)
    {
        this.item = item;
        title.text = item.name;
        quantity.text = $"x{GameController._GameController.GetInventoryItem(item)}";
        description.text = item.description;

        picture.sprite = Resources.Load<Sprite>($"UI/UITextures/{item.id}");

        button.onClick.AddListener(ClickItem);
    }

    private void ClickItem()
    {
        if(item.type == ItemType.PLACEABLE || item.type == ItemType.USEABLE)
        {
            GameController._GameController.SetHeldItem(this.item);
        }        
    }
}
