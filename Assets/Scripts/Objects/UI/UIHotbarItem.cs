﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHotbarItem : MonoBehaviour
{
    public Text text;
    public Image picture;
    public Image panel;
    private Item item;
    private int number;
    private static readonly Color colorNormal = new Color(0.23f, 0.23f, 0.23f);
    private static readonly Color colorHighlighted = new Color(0.43f, 0.43f, 0.43f);

    public void Start()
    {
        SetItemHighlighted(false);
    }

    public void SetNumber(int number)
    {
        this.number = number;
        text.text = number.ToString();
    }

    public void SetItem(Item item)
    {
        this.item = item;
        if(item != null)
        {
            picture.sprite = Resources.Load<Sprite>($"UI/UITextures/{item.id}");
        }
        picture.color = new Color(picture.color.r, picture.color.g, picture.color.b, (item == null) ? 0 : 255);
    }

    public Item GetItem()
    {
        return item;
    }

    public void SetItemHighlighted(bool highlighted)
    {
        panel.color = (highlighted) ? colorHighlighted : colorNormal;
        if(highlighted)
        {
            GameController._GameController._UIController.selectedHotbarItem = number - 1;
        }
    }
}
