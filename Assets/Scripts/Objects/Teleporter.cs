﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    public Transform target;
    private bool canTeleport;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == GameController._GameController.player.name)
        {
            canTeleport = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == GameController._GameController.player.name)
        {
            canTeleport = false;
        }
    }

    private void Update()
    {
        gameObject.transform.Rotate(new Vector3(0,Time.deltaTime * 20.0f, 0));

        if (canTeleport && Input.GetKeyDown(KeyCode.F))
        {
            Debug.Log("Teleporting player!");
            if(target)
            {
                GameController._GameController.player.transform.position = target.position;
            }
            else
            {
                Debug.LogError("No target for teleporter!");
            }
        }
    }
}
