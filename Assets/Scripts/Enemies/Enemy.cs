﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    private static Dictionary<EnemyColor, Color> colorMap = new Dictionary<EnemyColor, Color>();
    public EnemyType type;
    public EnemyColor color;
    public Transform spinnerBody;
    public Renderer glowPart;
    public NavMeshAgent agent;

    private Vector3 rotationChange = new Vector3(0, 10, 0); // Best range: 0 - 40

    private float health;
    private EnemyStatusEffect statusEffect;

    static Enemy()
    {        
        colorMap.Add(EnemyColor.PURPLE, new Color(1.0f, 0.0f, 1.0f));
        colorMap.Add(EnemyColor.BLUE, new Color(0.0f, 0.0f, 1.0f));
        colorMap.Add(EnemyColor.YELLOW, new Color(1.0f, 1.0f, 0.0f));
        colorMap.Add(EnemyColor.ORANGE, new Color(1.0f, 0.2f, 0.0f));
        colorMap.Add(EnemyColor.RED, new Color(1.0f, 0.0f, 0.0f));
    }

    private void Start()
    {
        GameController._GameController.enemies.Add(this);

        SetStatusEffect(EnemyStatusEffect.NONE);
        agent.SetDestination(GameController._GameController.enemyTarget.position);
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, GameController._GameController.enemyTarget.position) <= 1.5f) // TODO: Optimize this
        {
            if (GameController._GameController.gameState == GameState.GAME)
            {   
                if(!GameController._GameController.godMode)
                {
                    GameController._GameController.AddToPlayerStat(Stat.WALL_HEALTH, -1);
                }                
            }
            Remove();
        }
    }

    void FixedUpdate()
    {
        spinnerBody.Rotate(rotationChange);
    }

    public void SetSpeed(float speed)
    {
        Debug.Assert(agent != null);
        agent.speed = speed;
    }

    public void TakeDamage(float damage)
    {
        SetHealth(health - damage);
        
    }

    public void SetHealth(float newHealth)
    {
        health = newHealth;

        if (health <= 0)
        {
            this.Remove();
            GameController._GameController.AddToInventoryItem(0, BalanceDB.ENEMY_STATS[color].lifeDustYield);
        }        
    }

    public float GetHealth()
    {
        return health;
    }

    public void SetStatusEffect(EnemyStatusEffect statusEffect)
    {
        this.statusEffect = statusEffect;
        switch (statusEffect)
        {
            case EnemyStatusEffect.NONE:
                SetSpeed(BalanceDB.ENEMY_STATS[color].speed);
                SetMaterialColor(colorMap[color]);
                break;
            case EnemyStatusEffect.FROZEN:
                SetSpeed(BalanceDB.ENEMY_STATS[color].speed  * 0.40f);
                SetMaterialColor(new Color32(122, 242, 255, 255));
                Invoke("ResetStatus", 8.0f);
                break;
        }
    }

    public void ResetStatus()
    {
        SetStatusEffect(EnemyStatusEffect.NONE);
    }

    public void SetEnemyColor(EnemyColor color)
    {
        this.color = color;        
        SetMaterialColor(colorMap[color]);
        SetSpeed(BalanceDB.ENEMY_STATS[color].speed);
        SetHealth(BalanceDB.ENEMY_STATS[color].health);
    }

    public void SetMaterialColor(Color color)
    {
        glowPart.material.color = color;
        glowPart.material.SetColor("_EmissionColor", color);
    }

    public void Remove()
    {
        GameController._GameController.enemies.Remove(this);
        Destroy(gameObject);
    }
}

public enum EnemyType
{
    DEBUG, SPINNER
}

public enum EnemyColor
{
    PURPLE, BLUE, YELLOW, ORANGE, RED, WHITE
}

public enum EnemyStatusEffect
{
    NONE, FROZEN
}

public class EnemyStats
{
    public float speed;
    public float health;
    public int lifeDustYield;

    public EnemyStats(float speed, float health, int lifeDustYield)
    {
        this.speed = speed;
        this.health = health;
        this.lifeDustYield = lifeDustYield;
    }
}
